# apn switcher for mms
This is a workaround for Linux mobile users whose carrier has a separate APN for data and MMS. 

I have tested this with Pinephone, Librem 5 and Pinephone Pro and most of the OS that use Phosh (PMOS, Mobian, Arch, Manjaro)

To find out more about the original issue see:

https://gitlab.com/kop316/mmsd/-/issues/5

https://gitlab.freedesktop.org/NetworkMa...issues/958

## Description
If your carrier has different APN for data and MMS you can't receive MMS messages if you are using data APN and you can't use internet if you are using MMS APN. 

What this script does? 

It monitors ~/.mms/modemmanager/ folder and when it sees that there is new .status* file it changes to MMS APN and when it is done it changes back to DATA APN. Done can mean succes (aka. does the mmsd-tng remove .status file like it does when it succeeds) or error. 

It also sends notification if there is error and then it will save "raw mms" files to ~/.mms/error folder and you can try again just moving them back to ~/.mms/modemmanager/

*If there is new .status file you are getting or sending new MMS message -> this file is generated also when you are using data APN but it wil not be processed at all before switching to MMS APN.

## Installation
Most of the depencies are installed by default with most Phosh OSes

In terminal

git clone https://gitlab.com/Alaraajavamma/apn-switcher-for-mms && cd apn-switcher-for-mms && sudo chmod +x install.sh && sudo chmod +x mms.sh

System Settings you have to set both APNs correctly (seriously correctly - double check). Write down your APN names.

System Settings activate both once and check that they work

System Settings you have to leave the Data APN active

Chatty go to Settings -> SMS and MMS Settings -> and setup correctly MMS Carrier settings. Set these correctly and please double check. Also uncheck "Request delivery report" because otherwise this will cause notification loop with these scripts (atleast with my carrier)

Inside mms.sh you have to edit APN names so they match to your carrier APN names:


(This is not situation for most people but please notice (thanks b342)! Some carriers tend to have multiply APNs with same APN names. If that is your scenarioa you need to find out your carriers APN "UUID"
How? Select the ones that work from System Settings and when they work open terminal and run 'nmcli con show' that command will show you the UUIDs and what is the active APN UUID. And then you need to replace all "nmcli c up "dna internet" commands and replace them with: 'nmcli c up "UUID"' where UUID is the one what works with data or with mms - remember they are different if you here ;) )

Then run install.sh 
Reboot, login and wait a bit seconds - things should work now but please check :)

Uninstall is easy. Just remove start.desktop from .config/autostart and reboot

## Usage
When you follow "installation" instructions it should just work in the background

## Support
Feel free to open issue but and I can try to help. 
No warranty and that's it ;)

## Roadmap
This is just a workaround and will be total trash when someone smarter invents how Network Manager can handle multiply APN connections same time.
