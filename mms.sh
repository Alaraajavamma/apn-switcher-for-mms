#!/bin/bash

# Insert here your data apn name - make sure you have activated it first trough System Settings -> also make sure that this is the apn what you leave activated in System Settins
data_apn="internet"
# Insert here your mms apn name - make sure you have activated it at least once in System Settings. You need also to add this to Chatty and add asked configurations there
mms_apn="mms"

while :
do
	if [ -e /home/user/.mms/modemmanager/*.status ]; then 
nmcli c up $mms_apn && sleep 15 && nmcli c up $data_apn && sleep 30
fi
if [ -e /home/user/.mms/modemmanager/*.status ]; then 
notify-send MMS "Potential error when processing MMS - $(date +"%H:%M")" -i email
mv ~/.mms/modemmanager/*.status ~/.mms/error/
fi
sleep 5
done 
